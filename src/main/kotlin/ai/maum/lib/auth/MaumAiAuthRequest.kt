package ai.maum.lib.auth

import ai.maum.lib.auth.data.ClientCredentialsWrapper
import ai.maum.lib.auth.data.ClientToken
import ai.maum.lib.exceptions.MaumAiBaseException
import ai.maum.lib.exceptions.MaumAiResponseException
import ai.maum.lib.utils.HEADER_CONTENT_TYPE_JSON
import ai.maum.lib.utils.ILLEGAL_ARGUMENT_CLIENT_ID
import ai.maum.lib.utils.ILLEGAL_ARGUMENT_CLIENT_SECRET
import ai.maum.lib.utils.ILLEGAL_ARGUMENT_EMAIL
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.extensions.authentication
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import java.lang.IllegalArgumentException

/**
 * a singleton object with methods to use maum.ai api authorization.
 * uses fuel library to handle http calls.
 *
 * @author Jeeho Park <aquashdw>
 * @since 2020-11-30
 */
object MaumAiAuthRequest {
    // for production library
//    private const val authUrl = "https://aics-stg.maum.ai/auth"

    // for snapshot library
    var authUrl = "https://aics-stg.maum.ai/auth"

    private val logger = LoggerFactory.getLogger(this.javaClass) ?: null

    var clientId: String? = null
    var clientSecret: String? = null

    init {
        logger?.debug("maum.ai api auth handler instantiated")
    }

    fun setCredentials(clientId: String, clientSecret: String){
        logger?.debug("set credentials id: $clientId, secret: $clientSecret")
        this.clientId = clientId
        this.clientSecret = clientSecret
    }

    /**
     * returns ClientToken object based on singleton client id / key set
     * created for client applications (maintained by maum.ai clients)
     * <p>
     * For an outsider using a single client id / key set, the singleton object
     * of the concealing class keeps the client id / key for the entire application,
     * so that the client id / key does not have to be repeated for different parts of
     * the application.
     * @author  Jeeho Park <aquashdw>
     * @since 2020-11-30
     * @return  client token object which holds the token data received
     * @throws  IllegalArgumentException when any of this.clientId / this.clientKey is null
     * @throws  MaumAiResponseException when response status code is not 200 (success)
     * @see setCredentials()
     * */
    fun getAccessToken(): ClientToken {
        val response = this.requestToken(
                this.clientId, this.clientSecret
        )
        if(response.statusCode != 200) throw MaumAiResponseException(statusCode = response.statusCode)
        return Json.decodeFromString(response.body().asString(HEADER_CONTENT_TYPE_JSON))
    }

    /**
     * returns ClientToken object based on parameter client id / key set
     * created for service applications (maintained by maum.ai service teams)
     * <p>
     * A service application for maum.ai should be able to handle the client's id / secret more fluently.
     * Thus using the singleton's base id / key as it's own access method,
     * using this function the service may retrieve a client's id / key set.
     * <p>
     * There is no restriction that a client application may not use this function,
     * but if there are multiple points where an access token is needed, then it is recommended
     * to use getAccessToken() instead.
     *
     * @author  Jeeho Park <aquashdw>
     * @since 2020-11-30
     * @param   clientId clientId of token to be retrieved
     * @param   clientSecret clientSecret of token to be retrieved
     * @return  client token object which holds the token data received
     * @throws  IllegalArgumentException when any of parameter clientId / clientKey is null
     * @throws  MaumAiResponseException when response status code is not 200 (success)
     * @see getAccessToken
     * @see requestToken
     */
    fun getAccessToken(clientId: String?, clientSecret: String?): ClientToken{
        val response = this.requestToken(
                clientId, clientSecret
        )
        if(response.statusCode != 200) throw MaumAiResponseException(statusCode = response.statusCode)
        return Json.decodeFromString(response.body().asString(HEADER_CONTENT_TYPE_JSON))
    }

    /**
     * request to auth server for authentication.
     * works only if the application associated with the clientId / clientSecret
     * grant type is authorized with client_credentials.
     *
     * @author  Jeeho Park <aquashdw>
     * @since 2020-11-30
     * @param   clientId clientId of token to be retrieved
     * @param   clientSecret clientSecret of token to be retrieved
     * @throws  IllegalArgumentException when any of parameter clientId / clientKey is null
     * */
    private fun requestToken(clientId: String?, clientSecret: String?): Response{
        return Fuel.post(
                "$authUrl/oauth/token",
                listOf("grant_type" to "client_credentials"))
                .authentication()
                .basic(
                        clientId ?: throw IllegalArgumentException(ILLEGAL_ARGUMENT_CLIENT_ID),
                        clientSecret ?: throw IllegalArgumentException(ILLEGAL_ARGUMENT_CLIENT_SECRET)
                )
                .response().second
    }

    /**
     * returns ClientCredentialsWrapper object based on requested email.
     * created for service applications (maintained by maum.ai service teams)
     * <p>
     * An relationship of user - application - clientId is a 1:n:n kind of relationship.
     * Making a service front of a service (e.g. TTS Builder) requires the ability to lookup
     * the applications of the user that has logged in via maum.ai SSO.
     * With the integration with the maum.ai SSO allows to retrieve the email of the logged in user,
     * and thus the service application can use this function to retrieve the applications the user has created.
     *
     * The MaumAiAuthRequest object should be set with the service's own credentials (client id / secret)
     * as it is used to authenticate the retrieval of the user's applications.
     *
     * @author  Jeeho Park <aquashdw>
     * @since 2020-11-30
     * @param   email email of the client to lookup client credentials.
     * @throws  MaumAiBaseException when result returns null
     * @see     getAccessToken
     * */
    @Deprecated(message = "deprecated, auth admin functions are moved to cloud admin")
    fun getClientCredentials(email: String?): ClientCredentialsWrapper {
        val accessToken = this.getAccessToken().accessToken ?: throw MaumAiBaseException("received null access token from auth server")
        val response = Fuel.get("$authUrl/client", listOf("email" to (email ?: throw IllegalArgumentException(ILLEGAL_ARGUMENT_EMAIL))))
                .authentication()
                .bearer(accessToken)
                .response()
        if(response.second.statusCode != 200) throw MaumAiResponseException(statusCode = response.second.statusCode)
        return Json.decodeFromString(response.second.body().asString(HEADER_CONTENT_TYPE_JSON))
    }

    fun autoCreate(email: String){

    }

}