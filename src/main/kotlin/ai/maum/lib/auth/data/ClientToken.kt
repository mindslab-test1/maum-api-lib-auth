package ai.maum.lib.auth.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * ClientToken object.
 * Represents a single client token with relevant data.
 *
 * @author  Jeeho Park <aquashdw>
 * @since 2020-11-30
 * @param   accessToken the access token used for accessing maum.ai micro services
 * @param   tokenType the type of the token, the way to represent the token (e.g. bearer)
 * @param   expiresIn how long the token is valid in secs
 * @param   scope the scope of the token's authority
 * @param   jti
 */
@Serializable
data class ClientToken(
        @SerialName("access_token")
        val accessToken: String?,
        @SerialName("token_type")
        val tokenType: String?,
        @SerialName("expires_in")
        val expiresIn: Long?,
        val scope: String?,
        @SerialName("app_id")
        val appId: String?,
        @SerialName("app_owner")
        val appOwner: Long?,
        @SerialName("key_owner")
        val keyOwner: Long?,
        val jti: String?
)