package ai.maum.lib.auth.data

import kotlinx.serialization.Serializable

/**
 * A client credentials wrapper for response of auth server.
 * @see ClientCredentials
 * @see ai.maum.lib.auth.MaumAiAuthRequest.getClientCredentials
 * */
@Serializable
data class ClientCredentialsWrapper(
        val clients: List<ClientCredentials>?
)