package ai.maum.lib.auth.data

import kotlinx.serialization.Serializable

/**
 * CLientCredentials object.
 * Represents a client application's credential data.
 * <p>
 * A maum.ai service should create a application with well defined authorities and scope.
 * This objects represents a single application with details of the application's capability.
 *
 * @author  Jeeho Park <aquashdw>
 * @since 2020-11-30
 * @param   applicationName the name of the application.
 * @param   createType the method of how the application was created.
 * @param   clientId the client id of the application for MSA api.
 * @param   clientSecret the client key of the application for MSA api.
 * @param   scope list of scopes of the application.
 * @param   authorizedGrantTypes list of methods on how to retrieve access tokens.
 * @param`  authorities list of roles for the application.
 */
@Serializable
data class ClientCredentials(
        val applicationName: String?,
        val createType: String?,
        val clientId: String?,
        val clientSecret: String?,
        val scope: List<String>?,
        val authorizedGrantTypes: List<String>?,
        val authorities: List<String>?,
)