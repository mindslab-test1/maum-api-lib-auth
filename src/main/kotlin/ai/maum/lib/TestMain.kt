package ai.maum.lib

import ai.maum.lib.auth.MaumAiAuthRequest

fun main() {
    MaumAiAuthRequest.authUrl = "http://localhost:8080/auth"
    MaumAiAuthRequest.setCredentials(
            "f142b4a5-b07d-4ef2-891a-72ce475a2937",
            "dafc541d-215f-4ec7-9848-9b00a81ca788"
    )
    val resultToken = MaumAiAuthRequest.getAccessToken()
    println(resultToken)

//    val resultList = MaumAiAuthRequest.getClientCredentials("aquashdw@mindslab.ai")
//    println(resultList)
}